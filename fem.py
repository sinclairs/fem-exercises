#!/usr/bin/env python

# Exercise to implement a basic 2D FEM system

# Helpful: http://box2d.org/files/GDC2015/ErinCatto_NumericalMethods.pdf
# http://what-when-how.com/the-finite-element-method/fem-for-two-dimensional-solids-finite-element-method-part-1/

from matplotlib import pyplot as plt
import numpy as np
from multiprocessing import Process, Queue
import time

points = [(300,200)]*2
lines = [(0,1)]

white = (255,255,255)
black = (0,0,0)

# A visualisation window we open in a parallel process and communicate
# with through Queues
def show_points():
    import pygame
    import sys
    pygame.init()
    screen = pygame.display.set_mode((640,480))

    while (True):
        # check for quit events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return

        try:
            points = queue.get(True, timeout=0.033)
        except:
            continue

        # erase the screen
        screen.fill(black)

        # draw the updated picture
        for line in lines:
            line = [points[p] for p in line]
            pygame.draw.lines(screen, white, False, line, 2)
        for point in points:
            p = list(map(int,point))
            p[1] += 1 # centering
            pygame.draw.circle(screen, white, p, 5, 2)

        # update the screen
        pygame.display.update()

# Create the visual process only if it's not already open
if 'show_proc' in globals():
    if not show_proc.is_alive():
        del show_proc
if 'show_proc' not in globals():
    queue = Queue()
    show_proc = Process(target=show_points)
    show_proc.start()


if __name__=='__main__':
    x = 100.0
    v = 0.0
    k = 0.001
    t0 = t = time.time()
    while t - t0 < 10:
        #ddx = -kx
        x = x + v/2
        v = v - k*x
        x = x + v/2

        # update display
        points[1] = (x+300, 200)
        queue.put(points)

        # loop at 30 fps if possible
        t1 = time.time()
        if t1 - t < 0.033:
            time.sleep(t1-t+0.033)
        t = t1
