#!/usr/bin/env python

# Helpful: http://box2d.org/files/GDC2015/ErinCatto_NumericalMethods.pdf

from matplotlib import pyplot as plt
import numpy as np

N = 2000

# explicit method
x = 10
v = 0.0
y = np.zeros(N)
k = 0.001
for i in np.arange(N):
    #ddx = -kx
    a = -k*x
    v0 = v
    v += a
    x += v0
    y[i] = x
yexp = y


# implicit method (semi?)
x0 = 10
v0 = 0
y = np.zeros(N)
for i in np.arange(N):
    #ddx = -kx
    # v1 = v0-k*x1
    # x1 = x0+v1

    # solution
    v1 = v0/(1+k)-k*x0
    x1 = x0+v0/(1+k)

    y[i] = x1

    x0 = x1
    v0 = v1

yimp = y


# semi-implicit method (symplectic)
x = 10
v = 0.0
y = np.zeros(N)
for i in np.arange(N):
    #ddx = -kx
    a = -k*x
    v += a
    x += v
    y[i] = x
ysemi = y


# verlet-störmer method (central difference)
# https://web.stanford.edu/class/archive/physics/physics113/physics113.1142/cgi-bin/mediawiki/index.php/Second_Order_Differential_Equations
x = 10
v = 0.0
y = np.zeros(N)
for i in np.arange(N):
    #ddx = -kx
    x = x + v/2
    v = v - k*x
    x = x + v/2
    y[i] = x
yver = y

# analytical solution
yana = np.cos(np.arange(N)*np.sqrt(k))*10.0

plt.plot(yexp, label='explicit')
plt.plot(yimp, label='implicit')
plt.plot(ysemi, '--', label='semi-implicit')
plt.plot(yver, '-.', label='verlet')
plt.plot(yana, ':', label='analytical')
plt.legend()
plt.show()
